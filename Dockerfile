FROM python:2-alpine

WORKDIR /var/www/
EXPOSE 8080
CMD [ "python", "-m", "SimpleHTTPServer", "8080" ]
